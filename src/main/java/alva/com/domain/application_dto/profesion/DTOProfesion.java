package alva.com.domain.application_dto.profesion;

import java.util.Date;

//@Data
//@FieldDefaults(level = AccessLevel.PUBLIC)
public class DTOProfesion {
    public Integer idProfesion;
    public String profesion;
    public String detalle;
    public String estado;
    public String usuarioRegistro;
    public Date fechaRegistro;
    public String usuarioModifica;
    public Date fechaModifica;
}
