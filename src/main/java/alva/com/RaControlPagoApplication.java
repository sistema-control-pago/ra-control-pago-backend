package alva.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpringBootApplication
public class RaControlPagoApplication {
	private static final Logger logger = LogManager.getLogger(RaControlPagoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RaControlPagoApplication.class, args);
		logger.info("Se inicio la aplicación ;)");
	}

}
