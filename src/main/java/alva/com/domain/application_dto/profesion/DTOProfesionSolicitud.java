package alva.com.domain.application_dto.profesion;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DTOProfesionSolicitud {
    Integer idProfesion;
    String profesion;
    String detalle;
    String estado;
    String usuarioRegistro;
    String usuarioModifica;

    Integer page;
    Integer size;
    Integer totalElements;

}
