package alva.com.domain.application_dto.profesion;


import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class DTOProfesionResp {
    Integer page;
    Integer size;
    List<DTOProfesion> datos;
    Integer totalElements;
}
