package alva.com.infraestructure_data.repository;

import alva.com.domain.application_dto.profesion.DTOProfesion;
import alva.com.domain.application_dto.profesion.DTOProfesionSolicitud;
import alva.com.infraestructure_data.mapper.ProfesionMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

@Mapper
public interface ProfesionRepository {

    @SelectProvider(type = ProfesionMapper.class, method = "inicializar")
    List<DTOProfesion> inicializar(DTOProfesionSolicitud s);

    @SelectProvider(type = ProfesionMapper.class, method = "totalElemnts")
    Integer totalElemnts(DTOProfesionSolicitud s);

    @SelectProvider(type = ProfesionMapper.class, method = "agregar")
    void agregar(DTOProfesionSolicitud s);

    @SelectProvider(type = ProfesionMapper.class, method = "actualizar")
    void actualizar(DTOProfesionSolicitud s);

    @SelectProvider(type = ProfesionMapper.class, method = "cambiarEstado")
    void cambiarEstado(DTOProfesionSolicitud s);

    @SelectProvider(type = ProfesionMapper.class, method = "ver")
    DTOProfesion ver(Integer idProfesion);

}
