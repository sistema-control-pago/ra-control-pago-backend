package alva.com.distribuited_services.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import alva.com.domain.application_dto.rol.DTORolResp;
import alva.com.domain.application_dto.rol.DTORol;
import alva.com.domain.application_dto.rol.DTORolSolicitud;
import alva.com.domain.application_interfaces.RolService;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/controlpago/rol")
@CrossOrigin(origins = {"*"}, allowedHeaders = "*")
public class RolController {
    
    @Autowired
    private RolService rolService;

    @PostMapping("/inicializar")
    public RaRestServiceResponse<DTORolResp> inicializar(@RequestBody DTORolSolicitud s) {
        return rolService.inicializar(s);
    }

    @PostMapping("/agregar")
    public RaRestServiceResponse<DTORol> agregar(@RequestBody DTORolSolicitud s) {
        return rolService.agregar(s);
    }

    @PostMapping("/actualizar")
    public RaRestServiceResponse<DTORol> actualizar(@RequestBody DTORolSolicitud s) {
        return rolService.actualizar(s);
    }
    @PostMapping("/ver")
    public RaRestServiceResponse<DTORol> ver(@RequestBody DTORolSolicitud s) {
        return rolService.ver(s);
    }

    @PostMapping("/cambiarEstado")
    public RaRestServiceResponse<DTORol> cambiarEstado(@RequestBody DTORolSolicitud s) {
        return rolService.cambiarEstado(s);
    }
    
}
