package alva.com.domain.application_dto.rol;

import java.util.Date;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DTORol {
    public Integer idRol;
    String rol;
    String detalle;
    String estado;
    String usuarioRegistro;
    Date fechaRegistro;
    String usuarioModifica;
    Date fechaModifica;
}
