package alva.com.domain.application_services;

import alva.com.domain.application_dto.profesion.DTOProfesion;
import alva.com.domain.application_dto.profesion.DTOProfesionResp;
import alva.com.domain.application_dto.profesion.DTOProfesionSolicitud;
import alva.com.domain.application_interfaces.ProfesionAppService;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;
import alva.com.infraestructure_data.repository.ProfesionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProfesionAppServiceImpl implements ProfesionAppService {
    private static final Logger log = LogManager.getLogger(ProfesionAppServiceImpl.class);

    @Autowired
    private ProfesionRepository profesionRepository;

    public RaRestServiceResponse<DTOProfesionResp> inicializar(DTOProfesionSolicitud s) {
        RaRestServiceResponse<DTOProfesionResp> raRestServiceResponse = new RaRestServiceResponse<>();
        DTOProfesionResp respuesta = new DTOProfesionResp();
        try {
            respuesta.datos = profesionRepository.inicializar(s);
            respuesta.page = s.getPage();
            respuesta.size = s.getSize();
            respuesta.totalElements = profesionRepository.totalElemnts(s);;
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTOProfesion> agregar(DTOProfesionSolicitud s) {
        RaRestServiceResponse<DTOProfesion> raRestServiceResponse = new RaRestServiceResponse<>();
        DTOProfesion respuesta = new DTOProfesion();
        try {
            profesionRepository.agregar(s);
            respuesta.profesion = s.getProfesion();
            respuesta.detalle = s.getDetalle();
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTOProfesion> actualizar(DTOProfesionSolicitud s) {
        RaRestServiceResponse<DTOProfesion> raRestServiceResponse = new RaRestServiceResponse<>();
        DTOProfesion respuesta = new DTOProfesion();
        DTOProfesion datosBd = new DTOProfesion();
        try {
            datosBd = profesionRepository.ver(s.getIdProfesion());
            if(datosBd.getClass()==null){
                return raRestServiceResponse;
            }
            profesionRepository.actualizar(s);
            respuesta.profesion = s.getProfesion();
            respuesta.detalle = s.getDetalle();
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTOProfesion> cambiarEstado(DTOProfesionSolicitud s) {
        RaRestServiceResponse<DTOProfesion> raRestServiceResponse = new RaRestServiceResponse<>();
        DTOProfesion respuesta = new DTOProfesion();
        try {
            profesionRepository.cambiarEstado(s);
            respuesta.profesion = s.getProfesion();
            respuesta.detalle = s.getDetalle();
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTOProfesion> ver(DTOProfesionSolicitud s) {
        RaRestServiceResponse<DTOProfesion> raRestServiceResponse = new RaRestServiceResponse<>();
        DTOProfesion respuesta = new DTOProfesion();
        //DTOProfesion datosBd = new DTOProfesion();
        try {

            respuesta = profesionRepository.ver(s.getIdProfesion());
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }


}
