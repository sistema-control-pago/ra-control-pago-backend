package alva.com.domain.application_interfaces;

import alva.com.domain.application_dto.profesion.DTOProfesion;
import alva.com.domain.application_dto.profesion.DTOProfesionResp;
import alva.com.domain.application_dto.profesion.DTOProfesionSolicitud;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;

public interface ProfesionAppService {
    RaRestServiceResponse<DTOProfesionResp> inicializar(DTOProfesionSolicitud s);

    RaRestServiceResponse<DTOProfesion> agregar(DTOProfesionSolicitud s);

    RaRestServiceResponse<DTOProfesion> actualizar(DTOProfesionSolicitud s);

    RaRestServiceResponse<DTOProfesion> cambiarEstado(DTOProfesionSolicitud s);

    RaRestServiceResponse<DTOProfesion> ver(DTOProfesionSolicitud s);
}
