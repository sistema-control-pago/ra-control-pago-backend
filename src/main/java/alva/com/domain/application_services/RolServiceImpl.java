package alva.com.domain.application_services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import alva.com.domain.application_dto.rol.DTORolResp;
import alva.com.domain.application_dto.rol.DTORol;
import alva.com.domain.application_dto.rol.DTORolSolicitud;
import alva.com.domain.application_interfaces.RolService;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;
import alva.com.infraestructure_data.repository.RolRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Component
public class RolServiceImpl implements RolService {

    private static final Logger log = LogManager.getLogger(RolServiceImpl.class);

    @Autowired
    private RolRepository rolRepository;

    public RaRestServiceResponse<DTORolResp> inicializar(DTORolSolicitud s) {
        RaRestServiceResponse<DTORolResp> raRestServiceResponse = new RaRestServiceResponse<>();
        DTORolResp respuesta = new DTORolResp();
        try {
            respuesta.setDatos(rolRepository.inicializar(s));
            respuesta.setPage(s.getPage());
            respuesta.setSize(s.getSize());
            respuesta.setTotalElements(rolRepository.totalElemnts(s));
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTORol> agregar(DTORolSolicitud s) {
        RaRestServiceResponse<DTORol> raRestServiceResponse = new RaRestServiceResponse<>();
        DTORol respuesta = new DTORol();
        try {
            rolRepository.agregar(s);
            respuesta.setRol(s.getRol());
            respuesta.setDetalle(s.getDetalle());
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTORol> actualizar(DTORolSolicitud s) {
        RaRestServiceResponse<DTORol> raRestServiceResponse = new RaRestServiceResponse<>();
        DTORol respuesta = new DTORol();
        DTORol datosBd = new DTORol();
        try {
            datosBd = rolRepository.ver(s.getIdRol());
            if(datosBd.getClass()==null){
                return raRestServiceResponse;
            }
            rolRepository.actualizar(s);
            respuesta.setRol(s.getRol());
            respuesta.setDetalle(s.getDetalle());
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTORol> cambiarEstado(DTORolSolicitud s) {
        RaRestServiceResponse<DTORol> raRestServiceResponse = new RaRestServiceResponse<>();
        DTORol respuesta = new DTORol();
        try {
            rolRepository.cambiarEstado(s);
            respuesta.setRol(s.getRol());
            respuesta.setDetalle(s.getDetalle());
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }

    public RaRestServiceResponse<DTORol> ver(DTORolSolicitud s) {
        RaRestServiceResponse<DTORol> raRestServiceResponse = new RaRestServiceResponse<>();
        DTORol respuesta = new DTORol();
        try {

            respuesta = rolRepository.ver(s.getIdRol());
            raRestServiceResponse.content = respuesta;
        } catch (Exception ex) {
            raRestServiceResponse.SetException(ex);
            log.error(ex.getMessage(), ex);
        }
        return raRestServiceResponse;
    }
}
