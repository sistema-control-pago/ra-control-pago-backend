package alva.com.infraestructure_data.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;

import alva.com.domain.application_dto.rol.DTORol;
import alva.com.domain.application_dto.rol.DTORolSolicitud;
import alva.com.infraestructure_data.mapper.RolMapper;

@Mapper
public interface RolRepository {
    @SelectProvider(type = RolMapper.class, method = "inicializar")
    List<DTORol> inicializar(DTORolSolicitud s);

    @SelectProvider(type = RolMapper.class, method = "totalElemnts")
    Integer totalElemnts(DTORolSolicitud s);

    @SelectProvider(type = RolMapper.class, method = "agregar")
    void agregar(DTORolSolicitud s);

    @SelectProvider(type = RolMapper.class, method = "actualizar")
    void actualizar(DTORolSolicitud s);

    @SelectProvider(type = RolMapper.class, method = "cambiarEstado")
    void cambiarEstado(DTORolSolicitud s);

    @SelectProvider(type = RolMapper.class, method = "ver")
    DTORol ver(Integer idProfesion);
}
