package alva.com.domain.application_dto.rol;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DTORolSolicitud {
    public Integer idRol;
    String rol;
    String detalle;
    String estado;
    String usuarioRegistro;
    String usuarioModifica;

    Integer page;
    Integer size;
    Integer totalElements;
}
