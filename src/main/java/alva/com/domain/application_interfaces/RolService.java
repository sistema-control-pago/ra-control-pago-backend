package alva.com.domain.application_interfaces;

import alva.com.domain.application_dto.rol.DTORolResp;
import alva.com.domain.application_dto.rol.DTORol;
import alva.com.domain.application_dto.rol.DTORolSolicitud;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;

public interface RolService {

    RaRestServiceResponse<DTORolResp> inicializar(DTORolSolicitud s);

    RaRestServiceResponse<DTORol> agregar(DTORolSolicitud s);

    RaRestServiceResponse<DTORol> actualizar(DTORolSolicitud s);

    RaRestServiceResponse<DTORol> cambiarEstado(DTORolSolicitud s);

    RaRestServiceResponse<DTORol> ver(DTORolSolicitud s);
    
}
