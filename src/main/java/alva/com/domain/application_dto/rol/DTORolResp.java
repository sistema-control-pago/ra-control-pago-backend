package alva.com.domain.application_dto.rol;

import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DTORolResp {
    Integer page;
    Integer size;
    List<DTORol> datos;
    Integer totalElements;
}
