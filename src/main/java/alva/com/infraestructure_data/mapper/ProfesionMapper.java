package alva.com.infraestructure_data.mapper;


import alva.com.domain.application_dto.profesion.DTOProfesionSolicitud;
import org.apache.ibatis.jdbc.SQL;

public class ProfesionMapper {

    public String inicializar(DTOProfesionSolicitud s) {
        return new SQL() {
            {
                SELECT("idProfesion         idProfesion");
                SELECT("profesion           profesion");
                SELECT("detalle             detalle");
                SELECT("estado              estado");
                SELECT("usuario_registro    usuarioRegistro");
                SELECT("fecha_registro      fechaRegistro");
                SELECT("usuario_modifica    usuarioModifica");
                SELECT("fecha_modifica      fechaModifica");
                FROM("profesion");
                LIMIT(s.getSize());
                OFFSET(((long) s.getPage() *s.getSize()));

                if(s.getProfesion() != null){
                    WHERE(" profesion like '%" + s.getProfesion().trim()+ "%' ");
                }
                ORDER_BY("idProfesion DESC");

            }
        }.toString();
    }

    public String totalElemnts(DTOProfesionSolicitud s) {
        return new SQL() {
            {
                SELECT("COUNT(*)         idProfesion");
                FROM("profesion");
                if(s.getProfesion() != null){
                    WHERE(" profesion like '%" + s.getProfesion().trim()+ "%' ");
                }
            }
        }.toString();
    }


    public String agregar(DTOProfesionSolicitud s) {
        return new SQL() {
            {
                INSERT_INTO(" profesion ");
                VALUES("profesion",      " #{profesion , jdbcType=VARCHAR} ");
                VALUES("detalle",      " #{detalle , jdbcType=VARCHAR} ");
                VALUES("estado",      " '1' ");
                VALUES("usuario_registro",    " #{usuarioRegistro, jdbcType=VARCHAR} ");
                VALUES("fecha_registro",  " now()");
            }
        }.toString();
    }


    public String actualizar(DTOProfesionSolicitud s) {
        return new SQL() {
            {
                UPDATE(" profesion ");
                SET("profesion =  #{profesion , jdbcType=VARCHAR} ");
                SET("detalle =  #{detalle , jdbcType=VARCHAR} ");
                SET("usuario_modifica = #{usuarioModifica , jdbcType=VARCHAR}");
                SET("fecha_modifica = now()");
                WHERE("idProfesion = #{idProfesion , jdbcType=INTEGER}") ;
            }
        }.toString();
    }

    public String cambiarEstado(DTOProfesionSolicitud s) {
        return new SQL() {
            {
                UPDATE(" profesion ");
                SET("estado =  #{estado , jdbcType=VARCHAR} ");
                SET("usuario_modifica = #{usuarioModifica , jdbcType=VARCHAR}");
                SET("fecha_modifica = now()");
                WHERE("idProfesion = #{idProfesion , jdbcType=INTEGER}") ;
            }
        }.toString();
    }

    public String ver(Integer idProfesion) {
        return new SQL() {
            {
                SELECT("idProfesion         idProfesion");
                SELECT("profesion           profesion");
                SELECT("detalle             detalle");
                SELECT("estado              estado");
                SELECT("usuario_registro    usuarioRegistro");
                SELECT("fecha_registro      fechaRegistro");
                SELECT("usuario_modifica    usuarioModifica");
                SELECT("fecha_modifica      fechaModifica");
                FROM("profesion");
                WHERE(" idProfesion = #{idProfesion, jdbcType=INTEGER}");

            }
        }.toString();
    }


}
