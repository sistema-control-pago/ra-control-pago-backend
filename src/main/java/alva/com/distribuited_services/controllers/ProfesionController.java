package alva.com.distribuited_services.controllers;


import alva.com.domain.application_dto.profesion.DTOProfesion;
import alva.com.domain.application_dto.profesion.DTOProfesionResp;
import alva.com.domain.application_dto.profesion.DTOProfesionSolicitud;
import alva.com.domain.application_interfaces.ProfesionAppService;
import alva.com.infraestructure_cross_cuting.ra_response.RaRestServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/controlpago/profesion")
@CrossOrigin(origins = {"*"}, allowedHeaders = "*")
public class ProfesionController {

    @Autowired
    private ProfesionAppService profesionAppService;

    @PostMapping("/inicializar")
    public RaRestServiceResponse<DTOProfesionResp> Inicializar(@RequestBody DTOProfesionSolicitud s) {
        return profesionAppService.inicializar(s);
    }

    @PostMapping("/agregar")
    public RaRestServiceResponse<DTOProfesion> agregar(@RequestBody DTOProfesionSolicitud s) {
        return profesionAppService.agregar(s);
    }

    @PostMapping("/actualizar")
    public RaRestServiceResponse<DTOProfesion> actualizar(@RequestBody DTOProfesionSolicitud s) {
        return profesionAppService.actualizar(s);
    }
    @PostMapping("/ver")
    public RaRestServiceResponse<DTOProfesion> ver(@RequestBody DTOProfesionSolicitud s) {
        return profesionAppService.ver(s);
    }

    @PostMapping("/cambiarEstado")
    public RaRestServiceResponse<DTOProfesion> cambiarEstado(@RequestBody DTOProfesionSolicitud s) {
        return profesionAppService.cambiarEstado(s);
    }

}
