package alva.com.infraestructure_data.mapper;

import org.apache.ibatis.jdbc.SQL;

import alva.com.domain.application_dto.rol.DTORolSolicitud;

public class RolMapper {
    public String inicializar(DTORolSolicitud s) {
        return new SQL() {
            {
                SELECT("idRol               idRol");
                SELECT("rol                 rol");
                SELECT("detalle             detalle");
                SELECT("estado              estado");
                SELECT("usuario_registro    usuarioRegistro");
                SELECT("fecha_registro      fechaRegistro");
                SELECT("usuario_modifica    usuarioModifica");
                SELECT("fecha_modifica      fechaModifica");
                FROM("rol");
                LIMIT(s.getSize());
                OFFSET(((long) s.getPage() *s.getSize()));

                if(s.getRol() != null){
                    WHERE(" rol like '%" + s.getRol().trim()+ "%' ");
                }
                ORDER_BY("idRol DESC");

            }
        }.toString();
    }

    public String totalElemnts(DTORolSolicitud s) {
        return new SQL() {
            {
                SELECT("COUNT(*)         idRol");
                FROM("rol");
                if(s.getRol() != null){
                    WHERE(" rol like '%" + s.getRol().trim()+ "%' ");
                }
            }
        }.toString();
    }


    public String agregar(DTORolSolicitud s) {
        return new SQL() {
            {
                INSERT_INTO(" rol ");
                VALUES("rol",      " #{rol , jdbcType=VARCHAR} ");
                VALUES("detalle",      " #{detalle , jdbcType=VARCHAR} ");
                VALUES("estado",      " '1' ");
                VALUES("usuario_registro",    " #{usuarioRegistro, jdbcType=VARCHAR} ");
                VALUES("fecha_registro",  " now()");
            }
        }.toString();
    }


    public String actualizar(DTORolSolicitud s) {
        return new SQL() {
            {
                UPDATE(" rol ");
                SET("rol                =  #{rol , jdbcType=VARCHAR} ");
                SET("detalle            =  #{detalle , jdbcType=VARCHAR} ");
                SET("usuario_modifica   = #{usuarioModifica , jdbcType=VARCHAR}");
                SET("fecha_modifica     = now()");
                WHERE("idRol            = #{idRol , jdbcType=INTEGER}") ;
            }
        }.toString();
    }

    public String cambiarEstado(DTORolSolicitud s) {
        return new SQL() {
            {
                UPDATE(" rol ");
                SET("estado =  #{estado , jdbcType=VARCHAR} ");
                SET("usuario_modifica = #{usuarioModifica , jdbcType=VARCHAR}");
                SET("fecha_modifica = now()");
                WHERE("idRol = #{idRol , jdbcType=INTEGER}") ;
            }
        }.toString();
    }

    public String ver(Integer idRol) {
        return new SQL() {
            {
                SELECT("idRol               idRol");
                SELECT("rol                 rol");
                SELECT("detalle             detalle");
                SELECT("estado              estado");
                SELECT("usuario_registro    usuarioRegistro");
                SELECT("fecha_registro      fechaRegistro");
                SELECT("usuario_modifica    usuarioModifica");
                SELECT("fecha_modifica      fechaModifica");
                FROM("rol");
                WHERE(" idRol = #{idRol, jdbcType=INTEGER}");

            }
        }.toString();
    }
}
